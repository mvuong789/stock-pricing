import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from "@material-ui/core/Paper";

const SummaryComponent = (props) => {
  const { data } = props;
  let rows = [];

  data.forEach((value, key) => {
    rows.push(Object.assign({}, { key }, value));
  })

  return (
    <Paper className="summary-wrapper">
      <h1>Summary</h1>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Stock</TableCell>
              <TableCell>Starting</TableCell>
              <TableCell>Lowest</TableCell>
              <TableCell>Highest</TableCell>
              <TableCell>Current</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.key}>
                <TableCell component="th" scope="row">
                  {row.key}
                </TableCell>
                <TableCell>{row.starting}</TableCell>
                <TableCell>{row.lowest}</TableCell>
                <TableCell>{row.highest}</TableCell>
                <TableCell>{row.current}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}

export default SummaryComponent;
