import React, { useEffect } from 'react';

import LogComponent from './LogComponent';
import SummaryComponent from './SummaryComponent';

const StockPricesComponent = (props) => {
  const {
    displayedStockPrices, fetchStockPrices, isPaused, pauseStockPricing,
    parsedStockPrices, resumeStockPricing,
  } = props;

  const handleOnClick = () => {
    if (isPaused) {
      resumeStockPricing();
    } else {
      pauseStockPricing();
    }
  }

  useEffect(() => {
    const timeoutId = setInterval(() => {
      fetchStockPrices()
    }, 2000);

    return () => {
      clearInterval(timeoutId)
    }
  })

  return (
    <section className="stock-prices-wrapper">
      <LogComponent
        isPaused={isPaused}
        onClick={() => handleOnClick()}
        updates={displayedStockPrices}
      />
      <SummaryComponent
        data={parsedStockPrices}
      />
    </section>
  )
}

export default StockPricesComponent;
