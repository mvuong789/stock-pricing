import React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const LogComponent = (props) => {
  const { isPaused, onClick, updates } = props;

  let updateElements = [];

  if (updates.length > 0) {
    updateElements = updates.map((update, index) => {
      const { stockPrices, timeFetched } = update
      let stocks = []

      stocks = stockPrices.map((stock, stockIndex) => {
        const { code, price } = stock;

        return (
          <li key={`log-item-stock-${timeFetched}-${stockIndex}`}>
            {`${code}: $${price}`}
          </li>
        );
      })

      return (
        <div key={`log-item-${timeFetched}`} className="log-item">
          <div>{`Updates for ${timeFetched}`}</div>
          <ul className="log-item-stocks">
            {stocks}
          </ul>
        </div>
      );
    })
  }

  return (
    <Paper className="log-wrapper">
      <header className="log-header">
        <h1>Log</h1>
        <Button
          onClick={() => onClick()}
          variant="contained"
        >
          { isPaused ? 'Resume' : 'Pause' }
        </Button>
      </header>
      <section className="log-items-wrapper">
        {updateElements}
      </section>
    </Paper>
  )
}

export default LogComponent;
