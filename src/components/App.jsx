import React from 'react';

import StockPricesContainer from '@cont/StockPricesContainer';

const App = () => {
  return (
    <StockPricesContainer />
  )
}

export default App;
