import produce from 'immer';

import FIELDS from '@f/stockprices';
import TYPES from '@t/stockprices';

const {
  IS_FETCHING, IS_PAUSED, PARSED_STOCK_PRICES, STOCK_PRICES,
} = FIELDS;
const {
  PAUSE_STOCK_PRICING, RECEIVE_STOCK_PRICES, REQUEST_STOCK_PRICES,
  RESUME_STOCK_PRICING, SET_PARSED_STOCK_PRICES,
 } = TYPES;

const initialState = {
  [IS_FETCHING]: false,
  [IS_PAUSED]: false,
  [PARSED_STOCK_PRICES]: new Map(),
  [STOCK_PRICES]: [],
};

const reducer = (state = initialState, action) => {
  const { type, data } = action;
  return produce(state, draft => {
    switch (type) {
      case RECEIVE_STOCK_PRICES: {
        draft[STOCK_PRICES].unshift(data);
        draft[IS_FETCHING] = false;
        break;
      }
      case REQUEST_STOCK_PRICES: {
        draft[IS_FETCHING] = true;
        break;
      }
      case PAUSE_STOCK_PRICING: {
        draft[IS_PAUSED] = true;
        break;
      }
      case RESUME_STOCK_PRICING: {
        draft[IS_PAUSED] = false;
        break;
      }
      case SET_PARSED_STOCK_PRICES: {
        draft[PARSED_STOCK_PRICES] = data;
        break;
      }
      default:
        return draft;
    }
  });
};

export default reducer;
