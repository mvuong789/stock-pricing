import { combineReducers } from 'redux';

import reducer from './stockprices';
import FIELDS from '../fields/stockprices';

const { STOCK_PRICES } = FIELDS;

const rootReducer = combineReducers({
  [STOCK_PRICES]: reducer,
});

export default rootReducer;
