import fetch from 'cross-fetch';

import TYPES from '@t/stockprices';
import FIELDS from '@f/stockprices';

const {
  RECEIVE_STOCK_PRICES, REQUEST_STOCK_PRICES, PAUSE_STOCK_PRICING,
  RESUME_STOCK_PRICING, SET_PARSED_STOCK_PRICES,
} = TYPES;
const { STOCK_PRICES } = FIELDS;

export const receiveStockPrices = (data) => ({
  type: RECEIVE_STOCK_PRICES,
  data,
});

export const requestStockPrices = () => ({
  type: REQUEST_STOCK_PRICES,
});

export const pauseStockPricing = () => ({
  type: PAUSE_STOCK_PRICING,
});

export const resumeStockPricing = () => ({
  type: RESUME_STOCK_PRICING,
});

export const setParsedStockPrices = (data) => ({
  type: SET_PARSED_STOCK_PRICES,
  data,
});

export const fetchStockPrices = () => {
  return (dispatch, getState) => {
    dispatch(requestStockPrices());
    const { isPaused, parsedStockPrices } = getState()[STOCK_PRICES];

    const timeFetched = new Date(Date.now()).toLocaleString().replace(',', ' ');

    fetch('https://join.reckon.com/stock-pricing')
      .then((response) => response.json())
      .then((data) => {
        // Here we have the data returned from the api
        // We need to do 2 things:

        // 1. Set the data to display in the log
        dispatch(receiveStockPrices({
          stockPrices: data,
          hidden: isPaused,
          timeFetched,
        }));

        // 2. Parse the data to displayin the summary
        // Data is in this format: [ {"code": "ABC", price: 1}, ... ]
        // We will convert the data into a map of
        // code -> { starting, highest, lowest, current }
        const updatedMap = new Map();
        if (parsedStockPrices.size === 0) {
          data.forEach((stock) => {
            updatedMap.set(stock.code, {
              starting: stock.price,
              current: stock.price,
              highest: stock.price,
              lowest: stock.price,
            });
          });
        } else {
          data.forEach((stock) => {
            if (parsedStockPrices.has(stock.code)) {
              const current = parsedStockPrices.get(stock.code);
              const { highest, lowest } = current;
              updatedMap.set(
                stock.code,
                Object.assign(
                  {},
                  current,
                  {
                    current: stock.price,
                    highest: stock.price > highest ? stock.price : highest,
                    lowest: stock.price < lowest ? stock.price : lowest,
                  },
                ),
              );
            } else {
              updatedMap.set(stock.code, {
                starting: stock.price,
                current: stock.price,
                highest: stock.price,
                lowest: stock.price,
              });
            }
          });
        }

        dispatch(setParsedStockPrices(updatedMap));
      })
      .catch((error) => {
        console.error(error);
      });
  };
};

export default {
  receiveStockPrices,
  requestStockPrices,
  pauseStockPricing,
  resumeStockPricing,
  fetchStockPrices,
};
