import { connect } from 'react-redux';

import {
  fetchStockPrices, pauseStockPricing, resumeStockPricing,
} from '@a/stockprices';
import {
  getIsPaused, getDisplayedStockPrices, getParsedStockPrices,
} from '@s/stockprices';
import StockPricesComponent from '../components/StockPricesComponent';

const mapStateToProps = (state) => {
  const { stockPrices } = state;
  return {
    displayedStockPrices: getDisplayedStockPrices(stockPrices),
    isPaused: getIsPaused(stockPrices),
    parsedStockPrices: getParsedStockPrices(stockPrices),
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchStockPrices: () => {
    dispatch(fetchStockPrices());
  },
  pauseStockPricing: () => {
    dispatch(pauseStockPricing());
  },
  resumeStockPricing: () => {
    dispatch(resumeStockPricing());
  },
});

const StockPricesContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(StockPricesComponent);

export default StockPricesContainer;
