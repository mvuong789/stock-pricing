import { createSelector } from 'reselect';

import FIELDS from '@f/stockprices';

const {
  IS_FETCHING, IS_PAUSED, PARSED_STOCK_PRICES, STOCK_PRICES,
} = FIELDS;

export const getIsFetching = (state) => state[IS_FETCHING];

export const getIsPaused = (state) => state[IS_PAUSED];

export const getParsedStockPrices = (state) => state[PARSED_STOCK_PRICES];

export const getStockPrices = (state) => state[STOCK_PRICES];

export const getDisplayedStockPrices = createSelector(
  getStockPrices,
  (stockPrices) => stockPrices.filter((stock) => !stock.hidden),
);
