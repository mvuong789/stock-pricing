const IS_FETCHING = 'isFetching';
const IS_PAUSED = 'isPaused';
const PARSED_STOCK_PRICES = 'parsedStockPrices';
const STOCK_PRICES = 'stockPrices';

export default {
  IS_FETCHING,
  IS_PAUSED,
  PARSED_STOCK_PRICES,
  STOCK_PRICES,
};
