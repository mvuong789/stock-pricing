import TYPES from '@t/stockprices';
import {
  receiveStockPrices, requestStockPrices, pauseStockPricing, resumeStockPricing,
  setParsedStockPrices,
} from '@a/stockprices';

const {
  RECEIVE_STOCK_PRICES, REQUEST_STOCK_PRICES, PAUSE_STOCK_PRICING,
  RESUME_STOCK_PRICING, SET_PARSED_STOCK_PRICES,
} = TYPES;

describe('Testing stock prices actions', () => {
  it('should create an action to recieve stock prices', () => {
    const expectedAction = {
      type: RECEIVE_STOCK_PRICES,
      data: 'test',
    };

    expect(receiveStockPrices('test')).toEqual(expectedAction);
  });

  it('should create an action to request stock prices', () => {
    const expectedAction = {
      type: REQUEST_STOCK_PRICES,
    };

    expect(requestStockPrices()).toEqual(expectedAction);
  });

  it('should create an action to recieve stock prices', () => {
    const expectedAction = {
      type: PAUSE_STOCK_PRICING,
    };

    expect(pauseStockPricing()).toEqual(expectedAction);
  });

  it('should create an action to recieve stock prices', () => {
    const expectedAction = {
      type: RESUME_STOCK_PRICING,
    };

    expect(resumeStockPricing()).toEqual(expectedAction);
  });

  it('should create an action to set parsed stock prices', () => {
    const expectedAction = {
      type: SET_PARSED_STOCK_PRICES,
      data: 'test',
    };

    expect(setParsedStockPrices('test')).toEqual(expectedAction);
  });
});
