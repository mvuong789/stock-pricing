import {
  getIsFetching, getIsPaused, getParsedStockPrices, getStockPrices,
} from '@s/stockprices';

describe('Testing stock prices selectors', () => {
  const state = {
    isFetching: false,
    isPaused: false,
    parsedStockPrices: new Map(),
    stockPrices: ['test'],
  };

  it('should return isFetching when getIsFetching', () => {
    expect(getIsFetching(state)).toEqual(false);
  });

  it('should return isPaused when getIsPaused', () => {
    expect(getIsPaused(state)).toEqual(false);
  });

  it('should return stockPrices when getStockPrices', () => {
    expect(getStockPrices(state)).toEqual(['test']);
  });

  it('should return parsedStockPrices when getParsedStockPrices', () => {
    expect(getParsedStockPrices(state)).toEqual(new Map());
  });
});
