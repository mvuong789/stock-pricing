const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.jsx',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.styl(us)?$/,
        use: [
          'style-loader',
          'css-loader',
          'stylus-loader',
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ]
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx'
    ],
    alias: {
      '@': path.resolve(__dirname, 'src/'),
      '@a': path.resolve(__dirname, 'src/actions'),
      '@comp': path.resolve(__dirname, 'src/components'),
      '@cont': path.resolve(__dirname, 'src/containers'),
      '@cont': path.resolve(__dirname, 'src/containers'),
      '@f': path.resolve(__dirname, 'src/fields'),
      '@r': path.resolve(__dirname, 'src/reducers'),
      '@s': path.resolve(__dirname, 'src/selectors'),
      '@st': path.resolve(__dirname, 'src/styles/'),
      '@t': path.resolve(__dirname, 'src/types')
    }
  },
  devtool: 'source-map',
  devServer: {
    port: 9898,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: './index.html'
    })
  ],
};
