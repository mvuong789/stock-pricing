# Stock Pricing

This is a repo created for the interivew technical challenge for Reckon to display stock prices given data polled from an api.

I created this application via webpack, react, redux, redux-thunk, reselect, immer, babel, jest and material-ui. I decided to create my own application rather than use CRA because everything is under my control without extra bloat.

Hence the project architecture is in the simple Rails-style: separate folders for “actions”, “reducers”, “containers”, and “components”. However if this was a bigger application I may of opted to use the "Ducks" pattern.

# Run

Navigate to the repository location and run the following command: `yarn start`. This command will install related dependencies and build the application into the `/dist` folder.

After this process is complete, open the `index.html` file located within the `/dist` folder.
